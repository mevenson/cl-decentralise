(defsystem :decentralise
  :depends-on (:split-sequence
               :bordeaux-threads
	       :usocket
               :optima)
  :author "theemacsshibe"
  :license "Cooperative Software License v1+"
  :version "0.0.1"
  :components ((:module src
                        :components ((:file "package")
                                     (:file "macros")
                                     (:file "protocol")
                                     (:file "network")
                                     (:file "client")
                                     (:file "channels")
                                     (:file "eventhandler")
                                     (:file "sync")))))
