(defsystem decentralise-psql
  :depends-on (decentralise postmodern split-sequence)
  :components ((:module "psql"
                        :components ((:file "package")
                                     (:file "conditions")
                                     (:file "psql")))))
