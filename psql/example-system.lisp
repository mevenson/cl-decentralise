(defpackage :decentralise-psql-example
  (:use :cl :decentralise :decentralise-psql))

(in-package :decentralise-psql-example)

(defvar *system* (make-instance 'psql-system))
