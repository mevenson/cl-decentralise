(in-package :decentralise-psql)

(define-condition block-not-found (error)
  ((name :initarg :name
         :reader decentralise-block-name))
  (:report (lambda (condition stream)
             (format stream "~a not found"
                     (decentralise-block-name condition)))))
