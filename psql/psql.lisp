(in-package :decentralise-psql)

(defclass decentralise-row ()
  ((name :col-type varchar
         :initarg :name
         :initform ""
         :accessor row-name)
   (data :col-type varchar
         :initarg :data
         :initform ""
         :accessor row-data)
   (version :col-type int4
            :initarg :version
            :initform 0
            :accessor row-version)
   (channels :col-type varchar
             :initarg :channels
             :initform ""
             :accessor row-channels))
  (:metaclass dao-class)
  (:keys name))

(defclass psql-system (system))

(defun init-tables ()
  (loop for dao in '(decentralise-row)
     unless (table-exists-p (dao-table-name dao)) do
       (format *debug-io* "~&Creating table ~:(~a~) for ~:(~a~).~%"
               (dao-table-name dao)
               dao)
       (execute (dao-table-definition dao))))

(defun get-dao-row (name)
  (let ((dao (get-dao 'decentralise-row name)))
    (if (null dao)
        (error 'block-not-found :name name)
        dao)))

(declaim (inline channel-list-to-string channel-string-to-list))
(defun channel-list-to-string (list)
  (format nil "~{~a~^ ~}" list))
(defun channel-string-to-list (string)
  (let ((split (split-sequence #\Space string)))
    (unless (= (length split) 0)
      split)))
(defun create-item (name data version channels)
  (make-dao 'decentralise-row
            :name name
            :data data
            :version version
            :channels channels))

(defun update-item (dao data version channels)
  (setf (row-data dao) data
        (row-version dao) version
        (row-channels dao) channels)
  (update-dao dao))

(defmethod put-item ((system psql-system) name data version channels)
  (declare (ignore system))
  (let ((channels (channel-list-to-string channels)))
    (handler-case
        (get-dao-row name)
      (block-not-found ()
        (create-item name data version channels))
      (:no-error (dao)
        (update-item dao data version channels)))))

(defmethod get-item ((system psql-system) name)
  (declare (ignore system))
  (let ((dao (get-dao-row name)))
    (values (row-data dao)
            (row-version dao)
            (channel-string-to-list (row-channels dao)))))
        
(defmethod list-generator ((system psql-system))
  (declare (ignore system))
  (let ((name-versions
         (print (query (:select 'name 'version :from 'decentralise-row) :rows))))
    (lambda ()
      (if name-versions
          (destructuring-bind (name version)
              (pop name-versions)
            (values name version))
          (values nil nil)))))
