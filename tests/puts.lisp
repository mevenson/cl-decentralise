(in-package :decentralise-tests)

(def-suite value-tests
    :description "Testing the values accepted and returned by cl-decentralise.")
(in-suite value-tests)

(test test-empty-system
  (clrhash (data-table *system*))
  (clrhash (meta-table *system*))
  (is (test-output *system*
                   "GET list"
                   "BLOCK list 0
--EOF--")
      "The listing of an empty test-system is not empty"))

(test test-put
  (clrhash (data-table *system*))
  (clrhash (meta-table *system*))
  (is (test-output *system*
                   "BLOCK foo 1
I repeat myself when under stress.
--EOF--"
                   "OK foo")
      "Could not create a block.")
  (is (test-output *system*
                   "BLOCK foo 2
I boop myself when under stress.
--EOF--"
                   "ERROR foo No boops allowed!")
      "STOP-PUT-P did not stop the boop.")
  (is (test-output *system*
                   "BLOCK foo 3
I repeat myself when under stress.
--EOF--"
                   "OK foo")
      "Could not update a block with a newer version.")
  (is (test-output *system*
                   "BLOCK foo 2
I repeEEEEEEEEEEEEEAT!
--EOF--"
                   "ERROR foo too old")
      "Versioning did not stop the old block."))
  
(test test-get
  (clrhash (data-table *system*))
  (clrhash (meta-table *system*))
  (unless (test-output *system*
                       "BLOCK foo 1
His daughter
Was slated for becoming divine
He taught her
He taught her how to split and define
--EOF--"
                       "OK foo")
    (error "Could not put foo for test, what gives?"))
  (unless (test-output *system*
                       "BLOCK bar 2
But if you study the logistics
And heuristics of the mystics
You will find that their minds rarely move in a line
--EOF--"
                       "OK bar")
    (error "Could not put bar for test, what gives?"))
  (is (test-output *system* "GET foo"
                   "BLOCK foo 1
His daughter
Was slated for becoming divine
He taught her
He taught her how to split and define
--EOF--")
      "GET/BLOCK is not identical")
  (let ((listing (test-output *system* "GET list")))
    (is (and (search "foo 1" listing)
             (search "bar 2" listing))
        "Could not find `foo 1` and `bar 2` in the listing")))
