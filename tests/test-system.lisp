(in-package :decentralise-tests)

(defclass my-system (system)
  ((data-table :initform (make-hash-table :test 'equal)
               :accessor data-table)
   (meta-table :initform (make-hash-table :test 'equal)
               :accessor meta-table)))

(defmethod listing-generator ((system my-system))
  (let ((iterating t))
    (with-hash-table-iterator (iterator (meta-table system))
      (lambda ()
        (when iterating
          (multiple-value-bind (success name metadata)
              (iterator)
            (if success
                (values name (car metadata))
                (setf iterating nil))))))))

(defmethod put-item ((system my-system) name text version channels)
  (setf (gethash name (data-table system)) text
        (gethash name (meta-table system)) (cons version channels)))

(defmethod get-item ((system my-system) name)
  (multiple-value-bind (data win)
      (gethash name (data-table system))
    (if win
        (destructuring-bind (version &rest channels)
            (gethash name (meta-table system))
          (values data version channels))
        (error "not found"))))

(defmethod stop-put-p ((system my-system) name text version channels)
  (declare (ignore version channels name))
  (when (search "boop" text)
    "No boops allowed!"))

(defvar *system* (make-instance 'my-system :port 12345))
