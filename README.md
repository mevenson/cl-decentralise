# cl-decentralise: a decentralisation layer for your hacks

cl-decentralise is a minimal library for synchronising lists of string lines
(blocks) between machines. It is fully asynchronous, symmetrical and decentralised.

The protocol supports versioning and node discovery and lets you hack at the
rest of your system. You simply need to provide verification and storage
functions.

This repo is very experimental, and the internal representation is very likely
to change some time soon. The external symbols and function types shouldn't
change very soon though.

Documentation lives in the `doc/` directory. Here's some starting points:

- [how to write event loop bodies](https://gitlab.com/netfarm.gq/netfarm-docs/blob/master/cl-decentralise/2-eventloop.md)
- [how to write a cl-d system](https://gitlab.com/netfarm.gq/netfarm-docs/blob/master/cl-decentralise/3-logic.md)
- [how the protocol works](https://gitlab.com/netfarm.gq/netfarm-docs/blob/master/cl-decentralise/4-protocol.md)
