(defsystem :decentralise-tests
    :depends-on (:decentralise :fiveam :alexandria)
    :components ((:module tests
                          :components ((:file "package")
                                       (:file "test-system")
                                       (:file "macros")
                                       (:file "puts")
                                       (:file "run-tests")))))
