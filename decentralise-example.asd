(defsystem decentralise-example
  :depends-on (:decentralise)
  :components ((:module example
                        :components (:file "example"))))
