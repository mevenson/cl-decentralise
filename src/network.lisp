;;;; network.lisp: helper code for reading and writing blocks and text
;;; This file is a part of cl-decentralised.

(in-package :decentralise)

(defvar +eof-marker+ "--EOF--")

(defun split-by-spaces (line)
  (split-sequence #\Space line))

(defun connect-to-ip (ip &optional (port 1892))
  (let ((connection (make-instance 'connection :ip ip :port port)))
    (setf (connection-conn connection)
          (usocket:socket-stream
           (usocket:socket-connect ip port)))
    connection))

(declaim (inline read-conn-line))

(defun read-conn-line (connection)
  (read-line (connection-conn connection)))

(defun read-status-line (connection)
  (split-by-spaces
   (string-downcase
    (read-conn-line connection))))

(defun read-block (connection)
  (let ((status-line (read-status-line connection)))
    (if (string/= (first status-line) "block")
        ;; Errors don't have data.
        (error (format nil "~{~a~^ ~}" status-line))
        (values (read-rest-of-block connection)
                (second status-line)))))

(defun read-rest-of-block (connection)
  (with-output-to-string (output-stream)
    (loop
       for line = (read-conn-line connection)
       for first = nil then t
       until (string= line "--EOF--")
       when first
       do (terpri output-stream)
       do (write-string line output-stream))))

(defun write-text (connection text)
  (princ text (connection-conn connection))
  (terpri (connection-conn connection))
  (finish-output (connection-conn connection)))

(defun stringify-address (name)
  (if (stringp name) name
      (let ((lname (coerce name 'list)))
        (ecase (length lname) ; IPv4 or 6?
          (4 (format nil "~{~d~^.~}" lname))
          (6 (format nil "~{~d~^:~}" lname))))))
    
