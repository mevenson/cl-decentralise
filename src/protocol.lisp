;;;; protocol.lisp: structures & gfs cl-decentralise uses. not to be confused with network or client
;;; This file is a part of cl-decentralise.

(in-package :decentralise)

(defclass system ()
  ((connection-list :initform nil)
   (connection :initform nil)
   (port :initarg :port
         :initform 1892
         :reader system-port)
   (sync-limit :initarg :sync-limit
               :initform 70
               :accessor system-sync-limit))
  (:documentation "A decentralise system, a representation of (part of) a 
distributed program and its networking.
  :PORT can be used to change the port from the default of 1892, and
  :SYNC-LIMIT can be used to change the amount of nodes cl-decentralise will 
     track and connect to."))


(defclass connection ()
  ((ip :initform "" :initarg :ip :reader connection-ip)
   (port :initform 1892 :initarg :port :reader connection-port)
   (conn :initform nil :initarg :connection :accessor connection-conn))
  (:documentation "A representation of a connection to a decentralise instance.
This representation assumes that the client has an IP address and port; but
these may usually remain at their defaults or possibly unbound with no detriment
to the core of cl-decentralise.
  :IP sets the IP address of the client, and similarly
  :PORT sets the port.
  :CONN is the underlying stream the connection relies on."))

(defstruct connection-meta
  "The internal state of a connection, including the connection itself,
what blocks should be sent in the future (STACK), what channels the connection 
subscribed to (CHANNELS), a lock to prevent threading-related issues (LOCK), 
and a flag set if the node should be published in the NODES block (ANNOUNCEP)."
  socket
  (stack nil)
  (channels nil)
  (lock (bt:make-lock "connection-meta lock"))
  (announcep nil :type boolean))

(defgeneric listing-generator (system))
(defgeneric get-item (system name))
(defgeneric put-item (system name text version channels))
(defgeneric stop-put-p (system name text version channels))
(defmethod stop-put-p ((system system) name text version channels)
  (declare (ignore system name text version channels))
  nil)

(defvar *ip-blacklist* '("localhost" "127.0.0.1" "::1"))

;;; Synchronisation functions.

(defgeneric bind (system &key host))
(defgeneric write-object (system stream name))
(defgeneric handle-others-listing (system data meta))
(defgeneric handle-others-nodes (system text))
(defgeneric handle-new-block (name version text system connection meta channels))
(defgeneric handle-lines-from (connection system meta))
(defgeneric repl (connection system))
(defgeneric accept (system &key log))
(defgeneric connect-to (system host &key port timeout announcep))

;;; Network functions.

(defgeneric get-blocks (connection names))
(defgeneric write-block (connection name values &key version channels))
(defgeneric write-error (connection name &optional reason))
(defgeneric write-ok (connection name))
(defgeneric subscribe-channels (connection channels))
(defgeneric announce (connection &optional announcep))
(defgeneric read-event (connection))
