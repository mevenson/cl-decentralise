(in-package :decentralise)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-event-handler (event)
    (ematch event
      ((list* :get-blocks (list names) body)
       `((list :get ,names) ,@body))
      ((list* :new-block (list name version channels data) body)
       `((list :block ,name ,version ,channels ,data) ,@body))
      ((list* (list :new-block channel-filter) (list name version channels data) body)
       `((list :block ,name ,version
               (and ,channels
                    (satisfies (lambda (c) (should-send ,channel-filter c))))
               ,data)
         ,@body))
      ((list* :error (list name problem) body)
       `((list :error ,name ,problem) ,@body))
      ((list* :ok (list name) body)
       `((list :ok ,name) ,@body))
      ((list* :subscribe (list channels) body)
       `((list :subscribe ,channels) ,@body))
      ((list* :announce (list announcep) body)
       `((list :announce ,announcep) ,@body))
      ((list* :bad-syntax (list) body)
       `(nil ,@body)))))
  

(defmacro with-event-handlers (connection &body events)
  `(match (read-event ,connection)
     ,@(loop for event in events
          collect (make-event-handler event))))
