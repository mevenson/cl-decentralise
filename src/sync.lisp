;;;; package.lisp: synchronisation logic and daemon creation.
;;; This file is a part of cl-decentralise.

(in-package :decentralise)

(defmethod print-object ((system system) out)
  (print-unreadable-object (system out :type t)
    (with-slots (connection-list connection port) system
      (format out "~%~t:Connection-List ~a
~t:Connection ~a
~t:Port ~a" connection-list connection port))))

(defmethod bind ((system system) &key (host "0.0.0.0"))
  (setf (slot-value system 'connection)
        (socket-listen host
                       (slot-value system 'port)
                       :reuse-address t)))

(defun get-every-list (fn)
  (loop named list-collecting-loop
     with table = (make-hash-table :test 'equal)
     do (multiple-value-bind (name version)
            (funcall fn)
          (when (or (null name)
                    (null version))
            (return-from list-collecting-loop table))
          (setf (gethash name table) version))))

(defmethod write-object ((system system) stream name)
  (cond ((string= name "nodes")
         (write-block stream
                      "nodes"
                      (loop for node in (slot-value system 'connection-list)
                         when (connection-meta-announcep node)
                         unless (eql node system)
                         collect (connection-ip
                                  (connection-meta-socket
                                   node)))))
        ((string= name "list")
         (write-block stream
                      "list"
                      (loop
                         with table = (get-every-list (listing-generator system))
                         for name being the hash-keys of table
                         for version being the hash-values of table
                         collect (format nil "~a ~d" name version))))
        (t
         (let (data version channels)
           (handler-case
               (multiple-value-setq (data version channels)
                 (get-item system name))
             (condition (c)
               (write-error stream name
                            (princ-to-string c))
               (return-from write-object)))
           (write-block stream
                        name
                        data
                        :version version
                        :channels channels)))))


(defmethod handle-others-listing ((system system) data meta)
  (let* ((lock (connection-meta-lock meta))
         (connection (connection-meta-socket meta))
         (our-data (get-every-list (listing-generator system)))
         (their-data
          (loop
             with table = (make-hash-table :test 'equal)
             for item in (split-sequence #\Newline data :remove-empty-subseqs t)
             do (destructuring-bind (name version)
                    (split-sequence #\Space item)
                  (setf (gethash name table)
                        (parse-integer version)))
             finally (return table)))
         (give (loop
                  for name being the hash-keys in our-data
                  for version being the hash-values in our-data
                  for their-version = (gethash name their-data)
                  when (or (null their-version)
                           (> version their-version))
                  collect name))
         (take (loop
                  for name being the hash-keys in their-data
                  for their-version being the hash-values in their-data
                  for our-version = (gethash name our-data)
                  when (or (null our-version)
                           (> their-version our-version))
                  collect name)))
    (push-all give (connection-meta-stack meta))
    (bt:with-lock-held (lock)
      (get-blocks connection take))))

(defmethod handle-others-nodes (system text)
  (let* ((our-nodes
          (remove-duplicates
           (loop
              for node in (slot-value system 'connection-list)
              for socket = (slot-value node 'socket)
              for ip = (slot-value node 'ip)
              unless (member ip *ip-blacklist* :test #'string=)
              collecting ip)
           :test #'string=))
         (new-nodes (set-difference text our-nodes
                                    :test #'string=)))
    (loop
       for node in new-nodes
       while (< (length (slot-value system 'connection-list))
                (slot-value system 'sync-limit))
       do (bt:make-thread
           (lambda ()
             (ignore-errors
               (connect-to system (print node)
                           :timeout 2
                           :announcep t)))
           :name "Discovered node"))))

(defmethod handle-new-block (name version text (system system) connection meta channels)
  (cond
    ((string= name "list")
     (handle-others-listing system text meta))
    ((string= name "nodes")
     (handle-others-nodes system text))
    (t
     (let ((err (stop-put-p system name text version channels)))
       (cond
         (err
          (write-error connection name err))
         (t
          (let ((our-version
                 (ignore-errors
                   (nth-value 1 (get-item system name)))))
            (cond
              ((or
                (null our-version)
                (> version our-version))
               ;; if we have a new version: add it
               (put-item system name text version channels)
               (write-ok connection name)
               ;; tell the world about it
               (send-to-all-interested system meta name))
              (t
               ;; tell them they're wrong.
               (write-error connection name "too old")
               (when (should-send (connection-meta-channels meta)
                                  channels)
                 (push name (connection-meta-stack meta))))))))))))

(defmethod handle-lines-from (connection (system system) meta)
  (with-event-handlers connection
      (:new-block (name version-number channels data)
       ;; BLOCK (NAME) (VERSION) (CHANNELS*)
       (handle-new-block name version-number data
                         system connection meta channels))
      (:get-blocks (names)
       ;; GET (NAMES*)
       (push-all names (connection-meta-stack meta)))
      (:announce (announcep)
       ;; ANNOUNCE [yes|no]
       (setf (connection-meta-announcep meta) announcep))
      (:subscribe (subscriptions)
       ;; SUBSCRIBE (CHANNELS*)
       (setf (connection-meta-channels meta) subscriptions))
      (:bad-syntax ()
       (with-lock-held ((connection-meta-lock meta))
         (write-error connection "bad-syntax")))))
     
(defmethod repl (connection (system system))
  (let* ((lock (bt:make-lock "Write lock"))
         (meta (make-connection-meta :socket connection
                                     :stack nil
                                     :lock lock)))
    (push meta
          (slot-value system 'connection-list))
    (handler-case
         (progn
           (with-thread (:name "Connection listener")
             (loop
                (unless (null (connection-meta-stack meta))
                  (with-lock-held (lock)
                    (loop
                       for val = (pop
                                  (connection-meta-stack meta))
                       while val
                       do (write-object system connection val))))
                (sleep 0.1)))
           (loop
              (handle-lines-from connection system meta)))
      (end-of-file ()
        (format t "Byebye, ~a.~%" connection))
      #-debug 
      (condition (c)
        (format t "Killing ~a because ~a~%" connection c)
        (close (connection-conn connection))
        (setf (slot-value system 'connection-list)
              (delete meta
                      (slot-value system 'connection-list)
                      :test #'eq))))))

(defmethod accept ((system system) &key log (connection-type 'connection))
  (make-thread
   (lambda () 
     (loop
        for connection = (socket-accept (slot-value system 'connection))
        do (make-thread
            (lambda ()
              (repl (make-instance connection-type
                                   :ip (stringify-address
                                        (get-peer-address connection))
                                   :port (get-peer-port connection)
                                   :connection (socket-stream connection))
                    system)
              (socket-close connection)))
        when log
        do (format t "Connection from ~a:~d~%"
                   (get-peer-address connection)
                   (get-peer-port connection))))))

(defmethod connect-to ((system system) host &key
                                              (port (system-port system))
                                              (timeout 5)
                                              (announcep t)
                                              (subscriptions nil))
  (let* ((socket (socket-connect host port :timeout timeout))
         (raw-conn (socket-stream socket))
         (conn  (make-instance 'connection
                               :ip host
                               :port port
                               :connection raw-conn)))
    ;; ask for a file listing and all nodes
    (get-blocks conn '("list" "nodes"))
    (when announcep
      (announce conn t))
    (subscribe-channels conn subscriptions)
    (finish-output raw-conn)
    (unwind-protect
         (repl conn system)
      (socket-close socket))))
