;;;; package.lisp: the DEFPACKAGE.
;;; This file is a part of cl-decentralise.

(defpackage :decentralise
  (:use :cl :usocket :split-sequence :bordeaux-threads :optima)
  (:export :system
           ;; event handlers
           :with-event-handlers
           ;; sync
           :bind
           :accept
           :connect-to-ip
           ;; network
           :connection
           :connection-ip
           :connection-port
           :connection-conn
           ;; client
           :write-block
           :get-block
           :get-blocks
           :write-error
           :write-ok
           :subscribe-channels
           :announce
           ;; accessors
           :get-item
           :put-item
           :listing-generator
           :stop-put-p
           :system-sync-limit
           ;; readers
           :system-port))
