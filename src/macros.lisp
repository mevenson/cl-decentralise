;;;; macros.lisp: fancy lambda making macros.
;;; This file is a part of cl-decentralise.


(in-package :decentralise)

(defmacro then (&body parts)
  (let ((var (gensym "VAR")))
    `(lambda (,var)
       ,@(loop for part in parts
            collect (append part (list var))))))

(defmacro satisfy-all (&body tests)
  (let ((var (gensym "VAR")))
    `(lambda (,var)
       (and
        ,@(loop for part in tests
             collect (append part (list var)))))))

(defmacro push-all (values place)
  `(setf ,place
         (nconc ,values ,place)))

(defmacro with-thread ((&key (name "Thunk"))
                       &body body)
  `(bt:make-thread
    (lambda ()
      ,@body)
    :name ,name
    :initial-bindings `((*standard-output* . ,*standard-output*)
                        (*error-output* . ,*error-output*)
                        (*trace-output* . ,*trace-output*)
                        (*query-io* . ,*query-io*))))
